//
// Created by hyungook on 14. 12. 30..
// Copyright (c) 2014 Chope. All rights reserved.
//

#import "EntityUtils.h"
#import "BKAccount.h"
#import "NSDictionary+ChopeValue.h"
#import "BKUser.h"
#import "BKMatch.h"


@implementation EntityUtils

+ (BKAccount *)accountFromJson:(NSDictionary *)json {
    BKAccount *account = [[BKAccount alloc] init];
    account.accountId = [json unsignedIntegerForKey:@"id"];
    account.userId = [json unsignedIntegerForKey:@"userId"];
    account.email = [json stringForKey:@"email"];
    account.deleted = [json boolForKey:@"deleted"];
    return account;
}

+ (BKUser *)userFromJson:(NSDictionary *)json {
    BKUser *user = [[BKUser alloc] init];
    user.userId = [json unsignedIntegerForKey:@"id"];
    user.nickname = [json stringForKey:@"nickname"];
    user.photo = [json stringForKey:@"photo"];
    return user;
}

+ (BKMatch *)matchFromJson:(NSDictionary *)json {
    BKMatch *match = [[BKMatch alloc] init];
    match.matchId = [json unsignedIntegerForKey:@"id"];
    match.userId = [json unsignedIntegerForKey:@"userId"];
    match.round = [json unsignedIntegerForKey:@"round"];
    match.candidateA = [[self class] userFromJson:json[@"candidateA"]];
    match.candidateB = [[self class] userFromJson:json[@"candidateB"]];
    return match;
}

@end