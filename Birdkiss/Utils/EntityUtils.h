//
// Created by hyungook on 14. 12. 30..
// Copyright (c) 2014 Chope. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BKAccount;
@class BKUser;
@class BKMatch;


@interface EntityUtils : NSObject

+ (BKAccount *)accountFromJson:(NSDictionary *)json;
+ (BKUser *)userFromJson:(NSDictionary *)json;
+ (BKMatch *)matchFromJson:(NSDictionary *)json;

@end