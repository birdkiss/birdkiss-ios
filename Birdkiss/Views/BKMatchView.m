//
// Created by Chope on 14. 12. 30..
// Copyright (c) 2014 Chope. All rights reserved.
//

#import "BKMatchView.h"
#import "BKProfileCardView.h"
#import "BKMatch.h"
#import "UIView+ChopeView.h"

@interface BKMatchView ()

@property (nonatomic, strong) BKProfileCardView *topProfileView;
@property (nonatomic, strong) BKProfileCardView *bottomProfileView;
@property (nonatomic, strong) UILabel *waitingLabel;
@property (nonatomic, strong) UIButton *choiceButton;

@end


@implementation BKMatchView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self configureView];
    }

    return self;
}

- (void)configureView {
    self.clipsToBounds = YES;

    self.topProfileView = [[BKProfileCardView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [self addSubview:self.topProfileView];

    self.bottomProfileView = [[BKProfileCardView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [self addSubview:self.bottomProfileView];

    self.choiceButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.choiceButton.frame = CGRectMake(0, 0, 100, 100);
    self.choiceButton.center = self.center;
    self.choiceButton.backgroundColor = [UIColor blueColor];
    self.choiceButton.userInteractionEnabled = YES;
    [self.choiceButton.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
    [self.choiceButton addTarget:self action:@selector(moveChoiceButton:withEvent:) forControlEvents:UIControlEventTouchDragOutside];
    [self.choiceButton addTarget:self action:@selector(moveChoiceButton:withEvent:) forControlEvents:UIControlEventTouchDragInside];
    [self.choiceButton addTarget:self action:@selector(exitChoiceButton:withEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.choiceButton addTarget:self action:@selector(exitChoiceButton:withEvent:) forControlEvents:UIControlEventTouchUpOutside];
    [self.choiceButton setRound:50];
    [self addSubview:self.choiceButton];

    self.waitingLabel = [[UILabel alloc] initWithFrame:self.bounds];
    self.waitingLabel.text = @"Waiting...";
    self.waitingLabel.textAlignment = NSTextAlignmentCenter;
    self.waitingLabel.textColor = [UIColor whiteColor];
    self.waitingLabel.font = [UIFont systemFontOfSize:30];
    self.waitingLabel.hidden = YES;
    self.waitingLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    [self addSubview:self.waitingLabel];

    [self resetProfileViewFrame];
}

- (void)moveChoiceButton:(UIButton *)moveChoiceButton withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint point = [touch locationInView:self];

    [self.choiceButton setCenterY:point.y];

    if (point.y < self.center.y) {
        [self.bottomProfileView setY:point.y];
        [self.bottomProfileView setHeight:(self.bounds.size.height - point.y)];
        [self.topProfileView setY:(point.y - self.center.y)];
    } else {
        [self.topProfileView setHeight:point.y];
        [self.bottomProfileView setY:point.y];
    }

    if ([self isValidPoint:point.y]) {
        self.choiceButton.backgroundColor = [UIColor redColor];
    } else {
        self.choiceButton.backgroundColor = [UIColor blueColor];
    }
}

- (void)exitChoiceButton:(UIButton *)moveChoiceButton withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint point = [touch locationInView:self];
    CGFloat y = point.y;

    if ([self isValidPoint:y]) {
        self.choiceButton.backgroundColor = [UIColor redColor];
        self.waitingLabel.hidden = NO;
        self.waitingLabel.alpha = 0.0f;

        [UIView animateWithDuration:0.5 animations:^{
            if ([self isValidPointForTop:y]) {
                [self.topProfileView setY:(self.bounds.size.height * -1)];
                self.bottomProfileView.frame = self.bounds;
            } else {
                self.topProfileView.frame = self.bounds;
                [self.bottomProfileView setY:self.bounds.size.height];
            }

            self.choiceButton.alpha = 0.0f;
            self.waitingLabel.alpha = 1.0f;
        } completion:^(BOOL finished){
            self.choiceButton.hidden = YES;
        }];
    } else {
        self.choiceButton.backgroundColor = [UIColor blueColor];

        [UIView animateWithDuration:0.5 animations:^{
            [self.choiceButton setCenterY:self.center.y];

            [self resetProfileViewFrame];
        }];
    }
}

- (void)resetProfileViewFrame {
    CGRect frame = self.bounds;
    frame.size.height /= 2;

    self.topProfileView.frame = frame;

    frame.origin.y = frame.size.height;
    self.bottomProfileView.frame = frame;
}

- (void)setMatch:(BKMatch *)match {
    _match = match;

    if (match.candidateA == match.winner) {
        [self.topProfileView setHeight:self.frame.size.height];
        self.waitingLabel.hidden = NO;
    } else if (match.candidateB == match.winner) {
        self.bottomProfileView.frame = self.bounds;
        self.waitingLabel.hidden = NO;
    } else {
        [self resetProfileViewFrame];
    }

    [self.topProfileView setUser:match.candidateA];
    [self.bottomProfileView setUser:match.candidateB];

    [self.choiceButton setTitle:[NSString stringWithFormat:@"%d", match.round] forState:UIControlStateNormal];
}

- (BOOL)isValidPoint:(CGFloat)y {
    return [self isValidPointForTop:y] || (y > (self.bounds.size.height - (self.center.y / 2)));
}

- (BOOL)isValidPointForTop:(CGFloat)y {
    return y < (self.center.y / 2);
}

@end