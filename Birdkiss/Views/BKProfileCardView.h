//
// Created by hyungook on 14. 12. 30..
// Copyright (c) 2014 Chope. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BKUser;
@class BKProfileCardView;

@protocol BKProfileCardViewDelegate

- (void)touchedProfileCardView:(BKProfileCardView *)profileCardView;

@end


@interface BKProfileCardView : UIView

@property (nonatomic, weak) id<BKProfileCardViewDelegate> delegate;

- (void)setUser:(BKUser *)user;

@end