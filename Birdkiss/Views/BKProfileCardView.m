//
// Created by hyungook on 14. 12. 30..
// Copyright (c) 2014 Chope. All rights reserved.
//

#import "BKProfileCardView.h"
#import "UIView+ChopeView.h"
#import "UIColor+ChopeColor.h"
#import "BKUser.h"
#import "UIImageView+WebCache.h"
#import "FaceImageView.h"
#import "UIView+WebCacheOperation.h"
#import "AFHTTPRequestOperationManager.h"

@interface BKProfileCardView ()

@property (nonatomic, strong) UIImageView *photoImageView;
@property (nonatomic, strong) UIView *infoView;
@property (nonatomic, strong) UILabel *nicknameLabel;
@property (nonatomic, strong) UILabel *roundLabel;

@end


@implementation BKProfileCardView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self configureView];
    }

    return self;
}

- (void)configureView {
    self.photoImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    self.photoImageView.clipsToBounds = YES;
    self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:self.photoImageView];

    self.infoView = [[UIView alloc] initWithFrame:self.bounds];
    [self.infoView setHeight:50];
    [self.infoView setY:(self.frame.size.height - self.infoView.frame.size.height)];
    self.infoView.backgroundColor = [UIColor colorWithIntegerRed:0 green:0 blue:0 alpha:0.6];
    [self addSubview:self.infoView];

    self.nicknameLabel = [[UILabel alloc] initWithFrame:self.infoView.bounds];
    [self.nicknameLabel setX:10];
    self.nicknameLabel.textColor = [UIColor whiteColor];
    [self.infoView addSubview:self.nicknameLabel];

    self.roundLabel = [[UILabel alloc] initWithFrame:self.infoView.bounds];
    [self.roundLabel setWidth:self.roundLabel.frame.size.width - 10];
    self.roundLabel.textColor = [UIColor whiteColor];
    self.roundLabel.textAlignment = NSTextAlignmentRight;
    [self.infoView addSubview:self.roundLabel];

    self.clipsToBounds = YES;
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];

    self.photoImageView.frame = self.bounds;

    self.infoView.frame = self.bounds;
    [self.infoView setHeight:50];
    [self.infoView setY:(self.frame.size.height - self.infoView.frame.size.height)];

    self.nicknameLabel.frame = self.infoView.bounds;
    [self.nicknameLabel setX:10];
}

- (void)setUser:(BKUser *)user {
    [self.photoImageView sd_setImageWithURL:[[NSURL alloc] initWithString:user.photo]];

    self.nicknameLabel.text = user.nickname;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];

    if (self.delegate != nil) {
        [self.delegate touchedProfileCardView:self];
    }
}


@end