//
// Created by Chope on 14. 12. 30..
// Copyright (c) 2014 Chope. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BKMatch;


@interface BKMatchView : UIView

@property (nonatomic, weak) BKMatch *match;

@end