//
// Created by hyungook on 14. 12. 30..
// Copyright (c) 2014 Chope. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BKUser;


@interface BKMatch : NSObject

@property (nonatomic, assign) NSUInteger matchId;
@property (nonatomic, assign) NSUInteger userId;
@property (nonatomic, assign) NSUInteger round;
@property (nonatomic, strong) BKUser *candidateA;
@property (nonatomic, strong) BKUser *candidateB;
@property (nonatomic, weak) BKUser *winner;

@end