//
// Created by hyungook on 14. 12. 30..
// Copyright (c) 2014 Chope. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BKUser : NSObject

@property (nonatomic, assign) NSUInteger userId;
@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, strong) NSString *photo;

@end