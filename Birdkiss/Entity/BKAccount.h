//
// Created by hyungook on 14. 12. 30..
// Copyright (c) 2014 Chope. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BKAccount : NSObject

@property (nonatomic, assign) NSUInteger accountId;
@property (nonatomic, assign) NSUInteger userId;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, assign) BOOL deleted;

- (BOOL)hasUserId;

@end