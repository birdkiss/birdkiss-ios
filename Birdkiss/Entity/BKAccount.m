//
// Created by hyungook on 14. 12. 30..
// Copyright (c) 2014 Chope. All rights reserved.
//

#import "BKAccount.h"


@implementation BKAccount

- (BOOL)hasUserId {
    return self.userId > 0;
}

@end