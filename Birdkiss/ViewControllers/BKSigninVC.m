//
// Created by hyungook on 14. 12. 30..
// Copyright (c) 2014 Chope. All rights reserved.
//

#import "BKSigninVC.h"
#import "NSString+Chope.h"
#import "AFHTTPRequestOperationManager.h"
#import "BKAccount.h"
#import "EntityUtils.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "NSString+BirdkissAPI.h"

@interface BKSigninVC ()

@property (nonatomic, weak) IBOutlet UITextField *emailTF;
@property (nonatomic, weak) IBOutlet UITextField *passwordTF;

@end


@implementation BKSigninVC

- (void)viewDidLoad {
    [super viewDidLoad];

}


- (IBAction)touchSignin:(UIButton *)button {
    if ([self.emailTF.text isEmpty] || [self.passwordTF.text isEmpty]) {
        return;
    }

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    NSDictionary *parameters = @{
            @"email": self.emailTF.text,
            @"pwd": self.passwordTF.text,
    };

    [[AFHTTPRequestOperationManager manager] POST:[@"/account/signin" insertHost] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *response = responseObject;

        if (response[@"error"] == nil) {
            BKAccount *account = [EntityUtils accountFromJson:response[@"account"]];
            AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
            appDelegate.account = account;

            UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mainVC"];
            appDelegate.window.rootViewController = viewController;
        } else {
            NSLog(@"%@", response[@"error"]);
        }

        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }                                                                                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"%@", error);
    }];
};

- (IBAction)touchSignup:(UIButton *)button {

}


@end