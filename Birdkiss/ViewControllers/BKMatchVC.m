//
// Created by hyungook on 14. 12. 30..
// Copyright (c) 2014 Chope. All rights reserved.
//

#import <ChopeLibrary/UIView+ChopeView.h>
#import "BKMatchVC.h"
#import "AFHTTPRequestOperationManager.h"
#import "AppDelegate.h"
#import "BKAccount.h"
#import "EntityUtils.h"
#import "BKMatch.h"
#import "BKMatchView.h"
#import "NSString+BirdkissAPI.h"

@interface BKMatchVC ()

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;

@property (nonatomic, strong) NSMutableArray *matches;
@property (nonatomic, strong) NSArray *matchViews;

@end


@implementation BKMatchVC

- (void)viewDidLoad {
    [super viewDidLoad];

    [self loadMatches];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self play];
}

- (void)loadMatches {
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;

    NSDictionary *parameters = @{
            @"userId": @(delegate.account.userId)
    };

    [[AFHTTPRequestOperationManager manager] GET:[@"/matches" insertHost] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *response = responseObject;
        NSArray *responseMatches = response[@"matches"];
        NSMutableArray *matches = [[NSMutableArray alloc] init];

        if (response[@"error"] == nil) {
            for (NSDictionary *matchDic in responseMatches) {
                BKMatch *match = [EntityUtils matchFromJson:matchDic];
                [matches addObject:match];
            }

            self.matches = matches;

            [self play];
        } else {
            NSLog(@"%@", response[@"error"]);
        }
    }                                                                        failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error);
    }];
}

- (void)play {
    for (UIView *view in self.scrollView.subviews) {
        [view removeFromSuperview];
    }

    NSMutableArray *matchViewArray = [[NSMutableArray alloc] init];
    UIView *contentView = [[UIView alloc] initWithFrame:self.scrollView.bounds];
    [contentView setWidth:(self.matches.count * contentView.frame.size.width)];
    [contentView setY:0];

    for (NSUInteger i=0; i<self.matches.count; i++) {
        BKMatchView *matchView = [[BKMatchView alloc] initWithFrame:self.view.bounds];
        [matchView setX:(i * self.view.bounds.size.width)];
        matchView.match = self.matches[i];
        [matchViewArray addObject:matchView];
        [contentView addSubview:matchView];

        NSLog(@"%d : %@", i, NSStringFromCGRect(matchView.frame));
    }

    NSLog(@"view : %@", NSStringFromCGRect(self.view.frame));
    [self.scrollView addSubview:contentView];
    self.scrollView.frame = self.view.bounds;
    self.scrollView.contentSize = contentView.frame.size;

    NSLog(@"contentView : %@", NSStringFromCGRect(contentView.frame));
    NSLog(@"scrollView : %@", NSStringFromCGRect(self.scrollView.frame));
    NSLog(@"scrollContentSize : %@", NSStringFromCGSize(self.scrollView.contentSize));
}

@end