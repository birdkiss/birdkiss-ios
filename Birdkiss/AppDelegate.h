//
//  AppDelegate.h
//  Birdkiss
//
//  Created by hyungook on 2014. 12. 30..
//  Copyright (c) 2014년 Chope. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BKAccount;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) BKAccount *account;

@end

